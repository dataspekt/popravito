Package popravito
=================

Package `popravito` provides access to data from
[Popravi.to](https://popravi.to/) project by Code for Croatia and Gong.

Installation
------------

To install the development version of this package, use:

    devtools::install_gitlab("dataspekt/popravito")

Usage
-----

Function `requests()` will fetch requests data and return a data frame:

    library(popravito)
    requests()
    #> # A tibble: 286 × 17
    #>    service_request_id status title          agency_responsi… requested_datetime 
    #>    <chr>              <chr>  <chr>          <chr>            <dttm>             
    #>  1 319                open   ZET info ekra… Grad Zagreb      2021-11-05 00:00:00
    #>  2 318                open   Neispravna ja… Grad Požega      2021-11-05 00:00:00
    #>  3 317                open   Prepuni konte… Grad Zagreb      2021-11-05 00:00:00
    #>  4 316                open   Rupa na držav… Hrvatska         2021-11-05 00:00:00
    #>  5 315                open   Rupe na ulazu… Grad Zagreb      2021-11-05 00:00:00
    #>  6 314                open   Stupić upozor… Grad Zagreb      2021-11-05 00:00:00
    #>  7 313                open   Neasfaltiran … Hrvatska         2021-11-05 00:00:00
    #>  8 312                open   Napušteno voz… Grad Zagreb      2021-11-05 00:00:00
    #>  9 311                open   Sređivanje pa… Grad Osijek      2021-11-05 00:00:00
    #> 10 309                open   Neasfaltirana… Grad Zagreb      2021-11-05 00:00:00
    #> # … with 276 more rows, and 12 more variables: agency_sent_datetime <dttm>,
    #> #   updated_datetime <dttm>, service_code <chr>, service_name <chr>, lat <dbl>,
    #> #   long <dbl>, description <chr>, detail <chr>, media_url <chr>,
    #> #   requestor_name <chr>, interface_used <chr>, comments_count <chr>

Function `services()` will fetch a list of services and return it as a
data frama:

    services()
    #> # A tibble: 34 × 5
    #>    description                         metadata service_code  service_name type 
    #>    <chr>                               <chr>    <chr>         <chr>        <chr>
    #>  1 Autocesta                           false    Autocesta     Autocesta    real…
    #>  2 Biciklistička signalizacija         false    Biciklističk… Biciklistič… real…
    #>  3 Biciklistička staza                 false    Biciklističk… Biciklistič… real…
    #>  4 Biciklistički parking               false    Biciklističk… Biciklistič… real…
    #>  5 Divlje odlagalište otpada (deponij) false    Divlje odlag… Divlje odla… real…
    #>  6 Državna cesta                       false    Državna cesta Državna ces… real…
    #>  7 Grafiti                             false    Grafiti       Grafiti      real…
    #>  8 Javna garaža                        false    Javna garaža  Javna garaža real…
    #>  9 Javna plaža / javni bazen           false    Javna plaža … Javna plaža… real…
    #> 10 Javna rasvjeta                      false    Javna rasvje… Javna rasvj… real…
    #> # … with 24 more rows

Function `discovery()` will print some API metadata:

    discovery()
    #> Changeset: 2011-04-08T00:00:00Z
    #> Contact: Send email to popravi.to@codeforcroatia.org.
    #> Max requests: 1000
